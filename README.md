Requirements:  
*Upgrade python/ pip (version 3.8 / 20.1.1<upgrade paths>  
*Install : RabbitMQ (https://www.rabbitmq.com/)  
*Install : OpenCV (https://opencv.org/) or better from our Artifactory<be sure You have installed dev version>  
*Install : Pika <pip install pika>  
*Install : Visual Studio<safe version : 2019>  
*Install : CMake <2.6 - safe version>  
*Install : OpenSSL <1_1_1g - safe version>  



STEPS:  
CPP:  
*Upload Paths in RabbitMQ.props and OpenCV x64.props<if it is necessary>  
*Upload ws2_32.lib path in RabbitMQ.props to Your ws2_32.lib from VS or add it directly to project properties > Linker> input  
*Build rabbitmq-c <instruction below>  
*Run rabbit server from start_rabbitmq_service.cmd  
  
  
PYTHON:  
install missing imports like queue, pytest for example  
  
  
Building rabbitmq-c:   
*Run vs command line as administrator  
*Go to rabbitmq-c folder  
*Type :  
    mkdir build && cd build  
    cmake --build .  
Build an Release.  
*Check if paths in RabbitMQ.props are correctly  
